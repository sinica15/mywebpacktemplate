const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const autoprefixer = require('autoprefixer');

//const request = require('request');

const PATHS = {
	source: path.join(__dirname, 'source'),
	build: path.join(__dirname, 'build')
};

module.exports = {
	entry: PATHS.source + '\\index.js',
	output: {
		path: PATHS.build,
		filename: '[name].js'
	},
	module: {
		rules: [
			{
				test: /\.scss$/,
      			use: ExtractTextPlugin.extract({
        			fallback: 'style-loader',
        			use: ['css-loader', 'sass-loader']
      			})
    		},
			{
                test: /\.css$/,
                use: ["style-loader", "css-loader", "postcss-loader"]
            }
		]
        
		
//		rules: [
//				{
//					test: /\.scss$/,
//					use: [
//						{
//							loader: 'style-loader'
//						},
//						{
//							loader: 'css-loader',
//							options: {
//								sourceMap: true
//							}
//						},
//						{
//							loader: 'postcss-loader',
//							options: {
//								plugins: [
//									autoprefixer({
//										browsers:['ie >= 8', 'last 4 version']
//									})
//								],
//								sourceMap: true
//							}
//						},
//						{
//							loader: 'sass-loader',
//							options: {
//								includePaths: [
//									helpers.root('src', 'styles', 'global'),
//								],
//								sourceMap: true
//							}
//						}
//					],
//				},
//			]	
		
	},
	plugins: [
		new HtmlWebpackPlugin({
			title: 'Webpack app'
		}),
		new ExtractTextPlugin('style.css')	
	]
	
};

